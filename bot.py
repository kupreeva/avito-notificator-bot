import redis
import json
from bs4 import BeautifulSoup
import requests
import schedule
import time
import re
import telebot
import multiprocessing


token = 'XXXXXX<your-token>XXXXXX'
bot = telebot.TeleBot(token)


@bot.message_handler(commands=['start'])
def handle_start(message):
    """handler for command start"""
    bot.send_message(message.chat.id, text='Welcome! Send me a link to the ad you want to track')


@bot.message_handler(commands=['delete_all'])
def handle_delete_all(message):
    """handler for deleting all user's ads"""
    dlt = delete_all(str(message.chat.id))
    bot.send_message(message.chat.id, text=dlt)


@bot.message_handler(commands=['see_my_ads'])
def handle_see_ads(message):
    """handler for showing all user's ads"""
    ads = show_all_ads(str(message.chat.id))
    bot.send_message(message.chat.id, text=ads)
    if ads != 'You have no ads':
        bot.send_message(message.chat.id, text='Want to delete all ads? Press /delete_all')


def show_all_ads(user_id):
    """function for showing all user's ads"""
    result = read_redis()

    user_dict = result[user_id]
    ads = []
    i = 0
    if not user_dict:
        res_text = 'You have no ads'
    else:
        for k, v in user_dict.items():
            i = i + 1
            if v == 'Цена не указана':
                list_item = '{}\n{}\n!NO PRICE for this ad!\nDo you want to delete it? Press /delete_{}\n\n'.format(i, k, i)
            else:
                list_item = '{}\n{}\nDo you want to delete this ad? Press /delete_{}\n\n'.format(i, k, i)
            ads.append(list_item)
        res_text = ''.join(ads)

    return res_text


def check_url(message):
    """function for checking if it is the link to avito web site"""
    tpl = '[\w/:.]{0,}avito\.ru[\w/_\.\-?=]{0,}\d+$'
    if re.search(tpl, message.text.lower()) is not None:
        # if it matches the regular expression; user's message is link to avito
        if 's_trg' not in message.text.lower():
            write_to_base(message.chat.id, message.text)
            return True
        else:
            return False
    else:
        # it doesn't match the regular expression; user's message is not link to avito
        return False


def delete_ad(id, msg):
    """function for deleting one ad from a list of ads"""
    chat_id = str(id)
    msg_text = msg
    result = read_redis()
    num_ad = int(msg_text[8])
    i = 0
    for k in result[chat_id].copy():
        i += 1
        if i == num_ad:
            del result[chat_id][k]

    write_redis(result)
    res = "Ad was deleted"
    return res


def check_delete(message):
    """function for checking if the command from user is /delete"""
    if message.text[0:7] == "/delete":
        return True
    else:
        return False


def delete_all(chat_id):
    """function for deleting all user's ads"""
    result = read_redis()
    for k in result[chat_id].copy():
        del result[chat_id][k]

    write_redis(result)
    res = 'You have no ads now'
    return res


@bot.message_handler(func=check_url)
def start_monitor_ticket(message):
    """user's message is link to avito"""
    bot.send_message(message.chat.id, text="Done! If the price of this item changes, you will receive a message.")


@bot.message_handler(func=check_delete)
def delete_ticket(message):
    """user's message is link to avito"""
    res = delete_ad(message.chat.id, message.text)
    bot.send_message(message.chat.id, text=res)

    ads = show_all_ads(str(message.chat.id))
    bot.send_message(message.chat.id, text='Updated list of ads')
    bot.send_message(message.chat.id, text=ads)


@bot.message_handler(content_types=["text"])
def repeat_all_messages(message):
    """user's message is any text"""
    bot.send_message(message.chat.id, text="Send me a link to the ad you want to track")


def get_html(url):
    """function for getting html page"""
    r = requests.get(url)
    return r.text


def get_price(html):
    """function for parsing the price from html page"""
    soup = BeautifulSoup(html, 'lxml')
    try:
        # if link is avito.ru...
        price = soup.find("span", class_="js-item-price").get_text()
    except AttributeError:
        try:
            # if link is m.avito.ru... (mobile avito)
            price = soup.find("span", class_="price-value").get_text()
            price = price[3:]
            price = price[:-7]
            print(price)
        except AttributeError:
            # if price is not defined
            price = soup.find("span", class_="price-value-string js-price-value-string").get_text()
            price = price[4:19]

    return price


def write_to_base(chat_id, text_message):
    """function for writting a new avito link to database """

    result = read_redis()

    chat_id = str(chat_id)
    if chat_id not in result:
        # if it is new user
        # instead of the price, we initially write zero in the database
        # The function "job" will write the correct value of the price
        result[chat_id] = {text_message: 0}
    else:
        # if we already knew that user
        result[chat_id].update({text_message: 0})

    write_redis(result)


def job():
    """function for checking if the price has changed"""
    result = read_redis()

    for key, value in result.items():
        for k, v in value.items():
            mhtml = get_html(k)
            mprice = get_price(mhtml)

            if mprice == v:
                print("Price didn't change")
            else:
                print("Price changed")
                if v == 0 or mprice == 'Цена не указана':
                    result[key][k] = mprice
                else:
                    # sending message to user "price changed"
                    result[key][k] = mprice
                    bot.send_message(key, text='The price changed!  New price is {}'.format(mprice))

    write_redis(result)


def job2():
    """function for shedule working"""
    while True:
        schedule.run_pending()
        time.sleep(1)


def read_redis():
    """function for reading all data stored in redis database"""

    # connect redis
    r = redis.StrictRedis(host='localhost', port=6379, db=0)
    # load data from redis to a variable result
    data = r.get('key')
    result = json.loads(data.decode('utf-8'))
    print(result)
    return result


def write_redis(dict_for_save):
    """function for saving changes in redis database"""

    # connect redis
    r = redis.StrictRedis(host='localhost', port=6379, db=0)
    # convert dictionary to json string
    rval = json.dumps(dict_for_save)
    # load data to redis
    r.set('key', rval)


def main():
    # function main isn't needed for the telegram bot
    # function main is used for running only helper functions

    #write_redis({'151439871':{'url2':23}})
    delete_item_from_dict()
    #change_redis()
    read_redis()


# The auxiliary functions are written below. They are not needed directly for the telegram bot


def change_redis():
    """helper function for changing some data stored in redis database"""
    r = redis.StrictRedis(host='localhost', port=6379, db=0)
    data = r.get('key')
    result = json.loads(data)
    result['151439871'].update({'url2':23})
    print(result)
    rval = json.dumps(result)
    r.set('key', rval)


def delete_item_from_dict():
    """helper function for deleting items for our dict stored in redis database"""
    r = redis.StrictRedis(host='localhost', port=6379, db=0)
    data = r.get('key')
    result = json.loads(data.decode('utf-8'))
    del result['151439871']['url2']
    rval = json.dumps(result)
    r.set('key', rval)


if __name__ == "__main__":
    schedule.every(1).to(2).minutes.do(job)

    # we made a separate process for the function "job"
    t = multiprocessing.Process(target=job2)
    t.start()

    bot.polling()
    #main()



