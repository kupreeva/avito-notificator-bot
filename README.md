# Avito Notificator Bot
The telegram bot for tracking price changes for your selected ads on the site avito.ru
#### Due to Avito company often changes frontend (The class in html that contains price changes as well) this bot may not work. You should change classes in code (function "get_price").
---
### Getting started
#### Mac Os
##### Prerequisites
It is presumed that you have installed python3, virtualenv
* Pull the git repository via

```
$ git clone https://gitlab.com/kupreeva/avito-notificator-bot.git
```

* Install Redis

```
$ brew install redis
```

To use Redis as a permanent storage, you need to edit the configuration file as follows

```
appendonly yes
```

Location of Redis configuration file on mac.

```
/usr/local/etc/redis.conf
```

Start Redis server using configuration file

```
$ redis-server /usr/local/etc/redis.conf
```
* Install BeautifulSoup, requests, schedule
```
$ pip install beautifulsoup4 lxml
$ pip install requests
$ pip install schedule
```

* Install pyTelegramBotAPI

```
$ pip install pyTelegramBotAPI
```

Then obtain an API token with @BotFather

```python
token = 'write here your token'
```

* Run bot.py

```
$ python3 bot.py
```

#### Ubuntu 16.04.4
##### Prerequisites
It is presumed that you have installed python3

Be sure that your system is up-to-date
```
$ sudo apt-get update
```
then
```
$ sudo apt-get upgrade
```
* Install Redis

```
$ sudo apt-get install redis-server
```
* Virtualenv

We need to figure out our python3 location, then create and activate the virtualenv
```
$ which python3
```
You will see something like that
```
/usr/bin/python3
```
Create a new virtualenv in directory called venv
```
$ virtualenv --python=/usr/bin/python3 venv
```
Activate the virtualenv
```
$ source ~/venv/bin/activate
```
Now we can install the redis-py Python package
```
$ pip install redis
```
To use Redis as a permanent storage, you need to edit the configuration file as follows

```
appendonly yes
```

Location of Redis configuration file on Ubuntu.
```
etc/redis.conf
```
Start Redis
```
$ redis-server &
```
* Pull the git repository via

```
$ git clone https://gitlab.com/kupreeva/avito-notificator-bot.git
```
* Install BeautifulSoup, requests, schedule

```
$ pip install beautifulsoup4 lxml
$ pip install requests
$ pip install schedule
```
* Install pyTelegramBotAPI

```
$ pip install pyTelegramBotAPI
```

Then obtain an API token with @BotFather

```python
token = 'write here your token'
```
* In function read_redis you should change line

```python
result = json.loads(data)
```
to the following

```python
result = json.loads(data.decode('utf-8'))
```
* Run bot.py

```
$ python3 bot.py
```
